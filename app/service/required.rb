class Required
    def visualize(cn = nil,element_name = ':commentsN')
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        comments = wn.get_comments
        result = get_publications(users, posts, comments)
        
        result = result.sort_by{|a| a[element_name]}.reverse
    end

    def get_publications(users, posts, comments)
        posts_by_users = posts.group_by { |post| post["userId"] }
        comments_by_posts = comments.group_by { |comments| comments["postId"] }

        
        u = users.reduce([]) do |nm ,user|
          temp = posts_by_users[user["id"]].map do | post|  
               {
                id: user["id"],
                title: post["title"],
                body: post["body"],
                name: user["name"],
                username: user["username"],
                comments: comments_by_posts[post["id"]].slice(0..2).map {|comment| comment["body"]  },
                commentsN: comments_by_posts[post["id"]].length
               }          
          end
          nm.push(temp)
          nm
        end
        u.flatten(1)        
    end
    def users
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        pre = []
        users.map do |us|
            i=0
            posts.map do |po|
                if po["userId"] == us["id"] 
                    i += 1
                end
            end
            pbu = {"username" => us["username"],"n" => i}
            pre.push(pbu)
            pre = pre.sort_by{|a| a['n']}.reverse
        end 
        pre
    end
    def trending(n = nil)
        top = []
        post = visualize
        ta = post.take(n.to_i)
        ta.map do |t|
            t = {"titulo" => t[:title],"texto" => t[:body],"usuario" => t[:username],"autor" => t[:name]}
            top.push(t)
        end
        top
    end
    def influencers
        inf = visualize nil,':id'
        acumm = ''
        temp = []
        flag = 0
        userid=''
        username=''
        i=0
        tc=0
        fi=0
        inf.map do |a|
            if i == 0 || acumm == a[:id]
                i += 1
                tc += a[:commentsN]
                acumm = a[:id]
                username=a[:username]
            else              
                if flag > 0
                    i += 1
                    tc += a[:commentsN].to_i
                end  
                flag += 1
                t = {"username" => username, "indice" => tc/i}
                temp.push(t)
                i=0
                tc=0
            end
            fi = a[:commentsN].to_i 
        end

        i += 1
        tc += fi     
        t = {"username" => username, "indice" => tc/i}
        temp.push(t)
        temp = temp.sort_by{|a| a["indice"]}.reverse
        result = temp.take(5)

        
    end

    
end