class Bonus
    def users
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        pre = []
        tp = []
        userid=''
        username=''
        users.map do |us|
            i=0
            tp = []
            posts.map do |po|
                if po["userId"] == us["id"] 
                    i += 1
                    userid = us["id"]
                    username = us["username"]
                    po = {i => po["id"]}
                    tp.push(po)
                end 
            end
            t = {"userid" => userid, "username" => username, "posts" => tp}            
            pre.push(t)  
        end 
        pre
    end

    def detail(ps)
        wn = WapiiNet.new
        posts = wn.get_posts
        comments = wn.get_comments
        apost = []
        acomments = []
        posts.map do |po|
            if ps == po["id"]
                t = {"title" => po["title"], "body" => po["body"]}
                apost.push(t)
            end
            apost
            
        end
        comments.map do |co|
            if ps == co["postId"]
                t = {"name" => co["name"], "bodyComments" => co["body"], "email" => co["email"]}
                acomments.push(t)
            end
            acomments
        end
        result = {"post" => apost, "acomments" =>acomments}
    end
end