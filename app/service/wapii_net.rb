class WapiiNet

    def initialize()
        @base_uri = ENV["WAPII_NET_BASE_URI"]
    end

    def get_users()
        consume "#{@base_uri}/users"
    end

    def get_posts(id = nil)
        consume "#{@base_uri}/posts/#{id}"
    end

    def get_posts_comments(id = nil)
        consume "#{@base_uri}/posts/#{id}/comments"
    end

    def get_comments()
        consume "#{@base_uri}/comments"
    end

    private 
    def consume(resource)
        resp = HTTP.get(resource)
        raise StandarError("Mensaje") if resp.status.client_error? || resp.status.server_error?        
        parse_response resp
    end

  
    def parse_response(resp)
        data = JSON.parse resp.body
    end
end