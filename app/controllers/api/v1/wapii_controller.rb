class Api::V1::WapiiController < ApplicationController
    def users
        us = Required.new
        users = us.users
        render json:users
    end
    def trending
        tr = Required.new
        trending = tr.trending(params[:n])
        render json:trending
    end
    def influencers
        inf = Required.new
        influencers = inf.influencers
        render json:influencers
    end
end