
![hein](https://icon-icons.com/icons2/1736/PNG/256/4043240-avatar-bad-breaking-chemisrty-heisenberg_113279.png)

# Objetivo
Evaluar Ruby on Rails como herramienta en la resolución de un problema concreto.
Se solicita utilizar RubyOnRails en su versión 5.0.0 o superior.
# Desafío:
Wapii con el fin de acercarse a los usuarios quiere crear un nuevo producto llamado
Wapii-Net, una pequeña red social en donde todos sus usuarios puedan interactuar
libremente, publicando mensajes cortos y comentarios sobre esas publicaciones.
Ya existe una primera implementación en etapa de prototipo de este producto, un servicio
que permite obtener la lista de usuarios registrados, publicaciones y comentarios.
A continuación se muestran los endpoints que conforman el servicio en cuestión:

https://jsonplaceholder.typicode.com/users

https://jsonplaceholder.typicode.com/posts

https://jsonplaceholder.typicode.com/posts/1

https://jsonplaceholder.typicode.com/posts/1/comments

https://jsonplaceholder.typicode.com/comments

Por lo que le es asignada a usted la tarea de continuar con dicho proyecto creando una
aplicación web que permita visualizar estos datos y exponer algunos puntos de servicio
requeridos por el área de administración y negocios.
Concretamente esta aplicación web debe contar con:

● Visualización paginada de publicaciones (10 por página)
Cada publicación debe contar con el nombre del autor, el título de la publicación, el
texto de la publicación, los primeros 3 comentarios y el número total de comentarios.

● Endpoint /users
Obtener la lista de usuarios ordenados decrecientemente por cantidad de
publicaciones
[nombre de usuario, cantidad de publicaciones]

● Endpoint /trending/:n
Obtener las n publicaciones con más comentarios
[título, texto (body), nombre de usuario autor]

● Endpoint /influencers
Obtener los 5 usuarios mas populares (mayor cantidad de comentarios, en la menor
cantidad de publicaciones)
[nombre de usuario, índice de popularidad]
Entregables:

● Enlace a repositorio público con la resolución del desafío.

● Enlace a aplicación web en caso de estar disponible (bonus)

### Bonus:
● Aplicación web desplegada y disponible.

● Manejo de ramas.

● Vista con detalle de usuarios (Usuario con enlace a todas sus publicaciones)

● Vista con detalle de publicaciones (Post con todos sus comentarios)

## Desarrollo



## Abordando el desafío 

Ya que en lo solicitado no se habla de persistencia de datos, realice el desarrollo sin DB, todo en memoria (aunque hubiera sido mas simple usar Acive Record para consutlar datos), para poder mostrar información en tiempo real sin encolar.

### Servicios consumidos

 Para poder consumir los endpoints de wapii net creé una entidad utilizando el concepto de patrones de diseño, un service object llamado wappi_net, para poder disponibilizar la información.(BASE URI en variable de entorno)

app/service/wapii_net.rb

```sh
class WapiiNet

    def initialize()
        @base_uri = ENV["WAPII_NET_BASE_URI"]
    end

    def get_users()
        consume "#{@base_uri}/users"
    end

    def get_posts(id = nil)
        consume "#{@base_uri}/posts/#{id}"
    end

    def get_posts_comments(id = nil)
        consume "#{@base_uri}/posts/#{id}/comments"
    end

    def get_comments()
        consume "#{@base_uri}/comments"
    end

    private 
    def consume(resource)
        resp = HTTP.get(resource)
        parse_response resp
    end

    private 
    def parse_response(resp)
        data = JSON.parse resp.body
    end
end

```

 
### requerimientos
 
 Para poder resolver las 4 solicitudes (1 vista y 3 endpoints) creé una entidad Required, donde hay 4 métodos que resuelven cada uno de ellos, con group_by y/o con anidados para mostrar diferencias.

 app/service/required.rb

```sh
class Required
    def visualize(cn = nil,element_name = ':commentsN')
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        comments = wn.get_comments
        result = get_publications(users, posts, comments)
        
        result = result.sort_by{|a| a[element_name]}.reverse
    end

    def get_publications(users, posts, comments)
        posts_by_users = posts.group_by { |post| post["userId"] }
        comments_by_posts = comments.group_by { |comments| comments["postId"] }

        
        u = users.reduce([]) do |nm ,user|
          temp = posts_by_users[user["id"]].map do | post|  
               {
                id: user["id"],
                title: post["title"],
                body: post["body"],
                name: user["name"],
                username: user["username"],
                comments: comments_by_posts[post["id"]].slice(0..2).map {|comment| comment["body"]  },
                commentsN: comments_by_posts[post["id"]].length
               }          
          end
          nm.push(temp)
          nm
        end
        u.flatten(1)        
    end
    def users
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        pre = []
        users.map do |us|
            i=0
            posts.map do |po|
                if po["userId"] == us["id"] 
                    i += 1
                end
            end
            pbu = {"username" => us["username"],"n" => i}
            pre.push(pbu)
            pre = pre.sort_by{|a| a['n']}.reverse
        end 
        pre
    end
    def trending(n = nil)
        top = []
        post = visualize
        ta = post.take(n.to_i)
        ta.map do |t|
            t = {"titulo" => t[:title],"texto" => t[:body],"usuario" => t[:username],"autor" => t[:name]}
            top.push(t)
        end
        top
    end
    def influencers
        inf = visualize nil,':id'
        acumm = ''
        temp = []
        flag = 0
        userid=''
        username=''
        i=0
        tc=0
        fi=0
        inf.map do |a|
            if i == 0 || acumm == a[:id]
                i += 1
                tc += a[:commentsN]
                acumm = a[:id]
                username=a[:username]
            else              
                if flag > 0
                    i += 1
                    tc += a[:commentsN].to_i
                end  
                flag += 1
                t = {"username" => username, "indice" => tc/i}
                temp.push(t)
                i=0
                tc=0
            end
            fi = a[:commentsN].to_i 
        end

        i += 1
        tc += fi     
        t = {"username" => username, "indice" => tc/i}
        temp.push(t)
        temp = temp.sort_by{|a| a["indice"]}.reverse
        result = temp.take(5)

        
    end

    
end
```


 
### Bonus
 
 Como bonus se se solicitaba dos vistas (users y detalles ), para eso implementé una entidad bonus con un  método para cada uno de ellos.

 app/service/bonus.rb

```sh
class Bonus
    def users
        wn = WapiiNet.new
        users = wn.get_users
        posts = wn.get_posts
        pre = []
        tp = []
        userid=''
        username=''
        users.map do |us|
            i=0
            tp = []
            posts.map do |po|
                if po["userId"] == us["id"] 
                    i += 1
                    userid = us["id"]
                    username = us["username"]
                    po = {i => po["id"]}
                    tp.push(po)
                end 
            end
            t = {"userid" => userid, "username" => username, "posts" => tp}            
            pre.push(t)  
        end 
        pre
    end

    def detail(ps)
        wn = WapiiNet.new
        posts = wn.get_posts
        comments = wn.get_comments
        apost = []
        acomments = []
        posts.map do |po|
            if ps == po["id"]
                t = {"title" => po["title"], "body" => po["body"]}
                apost.push(t)
            end
            apost
            
        end
        comments.map do |co|
            if ps == co["postId"]
                t = {"name" => co["name"], "bodyComments" => co["body"], "email" => co["email"]}
                acomments.push(t)
            end
            acomments
        end
        result = {"post" => apost, "acomments" =>acomments}
    end
end

```
 
### Anexos
 
 ● En el requerimiento no se habla sobre como paginar, así que utilice datatable de jquery sobre la gema de paginación will_paginate, para poder darle un poco mas de ordén a la tabla.

 ● el nav superior tiene los elementos del menú para ver las vistas.

### Especificaciones

● Rails version 6.0.3.2

● Docker

app Dockerfile

```sh

 FROM ruby:2.6.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

ENV RAILS_ROOT /var/www/wapii
RUN mkdir -p $RAILS_ROOT

WORKDIR $RAILS_ROOT

#ENV RAILS_ENV='production'
ENV RAILS_ENV='development'
#ENV RACK_ENV='development'

COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

RUN bundle install --jobs 20 --retry 5 --without development test

COPY . .
#RUN bundle exec rake assets:precompile

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]

```

web Dockerfile

```sh
FROM nginx

RUN apt-get update -q && apt-get -y install apache2-utils

ENV RAILS_ROOT /var/www/wapii

WORKDIR $RAILS_ROOT

RUN mkdir log

COPY public public

COPY docker/web/nginx.conf /tmp/docker.nginx

RUN envsubst '$RAILS_ROOT' < /tmp/docker.nginx > /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

```

nginx config

```sh
upstream rails_app {
    server app:80;
}

server {
    server_name wapii.alvaro-tapia.cl;
    
    root $RAILS_ROOT/public;
    index index.html;
    access_log $RAILS_ROOT/log/nginx.access.log;
    error_log $RAILS_ROOT/log/nginx.error.log;

    location ~ /\. {
       deny all;
    }
    location ~* ^.+\.(rb|log)$ {
       deny all;
    }
    
    location ~ ^/(assets|images|javascripts|stylesheets|swfs|system)/ {
       try_files $uri @rails;
       access_log off;
       gzip_static on;
       
       expires max;
       add_header Cache-Control public;
       
       add_header Last-Modified "";
       add_header ETag "";
       break;

    }

    location / {
       try_files $uri @rails;
    }

    location @rails {
       proxy_set_header X-REAL-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;

       proxy_set_header Host $http_host;
       proxy_redirect off;
       proxy_pass http://rails_app;
     }
}


```

Gemas extras Utilizadas

● 'swag', '~> 0.5.0'

● 'uri', '~> 0.10.0'

● 'bootstrap', '~> 5.0.0.alpha1'

● 'jquery-rails'

● 'rswag-api'

● 'rswag-ui'