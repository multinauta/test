Rails.application.routes.draw do
  root 'visualize#index'
  mount Rswag::Ui::Engine => "/api-docs"
  mount Rswag::Api::Engine => "/api-docs"

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get 'visualize', to: 'visualize#index'
  get 'users', to: 'user#index'
  get 'detail', to: 'detail#index'
  namespace :api do
    namespace :v1 do
      get 'users', to: 'wapii#users'
      get 'trending', to: 'wapii#trending'
      get 'influencers', to: 'wapii#influencers'
    end
  end
end
